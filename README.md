### Goal

TradeStockMatcher is a cutting-edge microservice designed to streamline and optimize the process of matching buy and sell orders in the stock market.

The pet project's goals are to help contributors develop skills:
- Hexagonal & Onion Architecture, 
- TDD;
- concurrency & multithreading;
- collections usage.

### Swagger Doc

- [api-docs](http://localhost:8080/v3/api-docs)
- [swagger-config](http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config)

### Actuator Link

- [actuator-main](http://localhost:8080/actuator)
- [actuator-metrics](http://localhost:8080/actuator/metrics)
- [actuator-health-check](http://localhost:8080/actuator/health)

### How to run locally

#### Requirements

- Java 21
- Spring Boot 3.2.*

#### VM Options
