package ot.polyackov.trade.stock.matcher;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import org.junit.jupiter.api.Test;

class MachineTest {

    @Test
    void runMatchingEngine0() {
        var input = List.of("INSERT,1,FFLY,BUY,12.2,5");


        var actual = Machine.runMatchingEngine(input);


        var expected = List.of(
            "===FFLY===",
            "BUY,12.2,5"
        );

        assertEquals(expected, actual);
    }

    @Test
    void runMatchingEngine1() {
        var input = List.of(
            "INSERT,1,FFLY,BUY,0.3854,5",
            "INSERT,2,ETH,BUY,412,31",
            "INSERT,3,ETH,BUY,410.5,27",
            "INSERT,4,DOT,SELL,21,8",
            "INSERT,11,FFLY,SELL,0.3854,4",
            "INSERT,13,FFLY,SELL,0.3853,6"
        );


        var actual = Machine.runMatchingEngine(input);


        var expected = List.of(
            "FFLY,0.3854,4,11,1",
            "FFLY,0.3854,1,13,1",
            "===DOT===",
            "SELL,21,8",
            "===ETH===",
            "BUY,412,31",
            "BUY,410.5,27",
            "===FFLY===",
            "SELL,0.3853,5"
        );

        assertEquals(expected, actual);
    }


    @Test
    void runMatchingEngine2() {
        var input = List.of(
            "INSERT,1,FFLY,BUY,12.2,5",
            "INSERT,2,FFLY,SELL,12.3,5",
            "INSERT,3,FFLY,SELL,12.3,5",
            "CANCEL,2"
        );


        var actual = Machine.runMatchingEngine(input);


        var expected = List.of(
            "===FFLY===",
            "SELL,12.3,5",
            "BUY,12.2,5"
        );

        assertEquals(expected, actual);
    }

    @Test
    void runMatchingEngine3() {
        var input = List.of(
            "INSERT,1,FFLY,BUY,12.2,5",
            "INSERT,2,FFLY,SELL,12.1,8"
        );


        var actual = Machine.runMatchingEngine(input);


        var expected = List.of(
            "FFLY,12.2,5,2,1",
            "===FFLY===",
            "SELL,12.1,3"
        );

        assertEquals(expected, actual);
    }

    @Test
    void runMatchingEngine4() {
        var input = List.of(
            "INSERT,1,FFLY,BUY,14.235,5",
            "INSERT,2,FFLY,BUY,14.235,6",
            "INSERT,3,FFLY,BUY,14.235,12",
            "INSERT,4,FFLY,BUY,14.234,5",
            "INSERT,5,FFLY,BUY,14.23,3",
            "INSERT,6,FFLY,SELL,14.237,8",
            "INSERT,7,FFLY,SELL,14.24,9",
            "CANCEL,1",
            "INSERT,8,FFLY,SELL,14.234,25"
        );


        var actual = Machine.runMatchingEngine(input);


        var expected = List.of(
            "FFLY,14.235,6,8,2",
            "FFLY,14.235,12,8,3",
            "FFLY,14.234,5,8,4",
            "===FFLY===",
            "SELL,14.24,9",
            "SELL,14.237,8",
            "SELL,14.234,2",
            "BUY,14.23,3"
        );

        assertEquals(expected, actual);
    }

    @Test
    void runMatchingEngine5() {
        var input = List.of(
            "INSERT,1,FFLY,BUY,45.95,5",
            "INSERT,2,FFLY,BUY,45.95,6",
            "INSERT,3,FFLY,BUY,45.95,12",
            "INSERT,4,FFLY,SELL,46,8",
            "UPDATE,2,46,3",
            "INSERT,5,FFLY,SELL,45.95,1",
            "UPDATE,1,45.95,3",
            "INSERT,6,FFLY,SELL,45.95,1",
            "UPDATE,1,45.95,5,",
            "INSERT,7,FFLY,SELL,45.95,1"
        );


        var actual = Machine.runMatchingEngine(input);


        var expected = List.of(
            "FFLY,46,3,2,4",
            "FFLY,45.95,1,5,1",
            "FFLY,45.95,1,6,1",
            "FFLY,45.95,1,7,3",
            "===FFLY===",
            "SELL,46,5",
            "BUY,45.95,16"
        );

        assertEquals(expected, actual);
    }

    @Test
    void runMatchingEngine6() {
        var input = List.of(
            "INSERT,1,FFLY,SELL,12.2,5",
            "INSERT,2,FFLY,SELL,12.1,8,",
            "INSERT,3,FFLY,BUY,12.5,10"
        );


        var actual = Machine.runMatchingEngine(input);


        var expected = List.of(
            "FFLY,12.1,8,3,2",
            "FFLY,12.2,2,3,1",
            "===FFLY===",
            "SELL,12.2,3"
        );

        assertEquals(expected, actual);
    }

    @Test
    void runMatchingEngine7() {
        var input = List.of(
            "INSERT,1,FFLY,BUY,120,7",
            "INSERT,2,FFLY,BUY,121,12",
            "INSERT,3,FFLY,BUY,121,23",
            "INSERT,4,FFLY,BUY,122,1",
            "INSERT,5,FFLY,SELL,120,16",
            "INSERT,6,FFLY,SELL,121,24"
        );


        var actual = Machine.runMatchingEngine(input);


        var expected = List.of(
            "FFLY,122,1,5,4",
            "FFLY,121,12,5,2",
            "FFLY,121,3,5,3",
            "FFLY,121,20,6,3",
            "===FFLY===",
            "SELL,121,4",
            "BUY,120,7"
        );

        assertEquals(expected, actual);
    }

    @Test
    void runMatchingEngine8() {
        var input = List.of(
            "INSERT,1,FFLY,BUY,45.95,5",
            "INSERT,2,FFLY,BUY,45.95,6",
            "INSERT,3,FFLY,BUY,45.95,12",
            "UPDATE,2,45.95,3",
            "INSERT,4,FFLY,SELL,45.95,9",
            "INSERT,5,FFLY,SELL,46,1",
            "UPDATE,3,46,3"
        );


        var actual = Machine.runMatchingEngine(input);


        var expected = List.of(
            "FFLY,45.95,5,4,1",
            "FFLY,45.95,3,4,2",
            "FFLY,45.95,1,4,3",
            "FFLY,46,1,3,5",
            "===FFLY===",
            "BUY,46,2"
        );

        assertEquals(expected, actual);
    }

    @Test
    void runMatchingEngine9() {
        var input = List.of(
            "INSERT,1,FFLY,SELL,45.95,5",
            "INSERT,2,FFLY,SELL,45.95,6",
            "INSERT,3,FFLY,SELL,45.95,12",
            "UPDATE,2,45.95,3",
            "INSERT,4,FFLY,BUY,45.95,9",
            "INSERT,5,FFLY,BUY,45,1",
            "UPDATE,3,45,3"
        );


        var actual = Machine.runMatchingEngine(input);


        var expected = List.of(
            "FFLY,45.95,5,4,1",
            "FFLY,45.95,3,4,2",
            "FFLY,45.95,1,4,3",
            "FFLY,45,1,3,5",
            "===FFLY===",
            "SELL,45,2"
        );

        assertEquals(expected, actual);
    }

    @Test
    void runMatchingEngine10() {
        var input = List.of(
            "INSERT,1,FFLY,BUY,47,5",
            "INSERT,2,FFLY,BUY,47,6",
            "INSERT,3,FFLY,SELL,47,9",
            "UPDATE,2,47,-1"
        );


        var actual = Machine.runMatchingEngine(input);


        var expected = List.of(
            "FFLY,47,5,3,1",
            "FFLY,47,4,3,2",
            "===FFLY===",
            "BUY,47,2"
        );

        assertEquals(expected, actual);
    }


    @Test
    void runMatchingEngine11() {
        var input = List.of(
            "INSERT,1,FFLY,BUY,47,5",
            "INSERT,2,FFLY,BUY,47,6",
            "INSERT,3,FFLY,SELL,47,9",
            "UPDATE,1,45,2",
            "UPDATE,5,45,2"
        );


        var actual = Machine.runMatchingEngine(input);


        var expected = List.of(
            "FFLY,47,5,3,1",
            "FFLY,47,4,3,2",
            "===FFLY===",
            "BUY,47,2"
        );

        assertEquals(expected, actual);
    }

    @Test
    void runMatchingEngine12() {
        var input = List.of(
            "INSERT,1,FFLY,BUY,47,5",
            "INSERT,2,FFLY,BUY,47,6",
            "INSERT,3,FFLY,SELL,47,9",
            "CANCEL,1"
        );


        var actual = Machine.runMatchingEngine(input);


        var expected = List.of(
            "FFLY,47,5,3,1",
            "FFLY,47,4,3,2",
            "===FFLY===",
            "BUY,47,2"
        );

        assertEquals(expected, actual);
    }


    @Test
    void runMatchingEngine13() {
        var input = List.of(
            "INSERT,1,FFLY,BUY,48,10",
            "INSERT,2,FFLY,BUY,48,6",
            "INSERT,3,FFLY,BUY,48,12",
            "INSERT,4,FFLY,SELL,46,7",
            "UPDATE,1,48,2",
            "INSERT,5,FFLY,SELL,46,5",
            "UPDATE,2,48,4",
            "INSERT,6,FFLY,SELL,46,1",
            "UPDATE,3,48,15",
            "INSERT,7,FFLY,SELL,46,2"
        );


        var actual = Machine.runMatchingEngine(input);


        var expected = List.of(
            "FFLY,48,7,4,1",
            "FFLY,48,2,5,1",
            "FFLY,48,3,5,2",
            "FFLY,48,1,6,3",
            "FFLY,48,2,7,2",
            "===FFLY===",
            "BUY,48,17"
        );

        assertEquals(expected, actual);
    }
}