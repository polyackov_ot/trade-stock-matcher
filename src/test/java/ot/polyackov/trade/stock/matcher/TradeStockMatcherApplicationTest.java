package ot.polyackov.trade.stock.matcher;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TradeStockMatcherApplicationTest {

    @Test
    void contextLoads() {
    }

}