package ot.polyackov.trade.stock.matcher.output;

import static java.lang.String.format;

import ot.polyackov.trade.stock.matcher.core.dto.Trade;


public class TradeConverter {

    /**
     * Convert {@link Trade} to CSV format.
     * <pre>
     * {@code <symbol>,<price>,<volume>,<taker_order_id>,<maker_order_id>}
     * E.g
     * {@code FFLY,14.235,6,8,2}
     * </pre>
     */
    public static String convertToCsv(Trade trade) {
        return format("%s,%s,%d,%d,%d",
            trade.getSymbol(),
            trade.getPrice().toString(),
            trade.getVolume(),
            trade.getTakerOrderId(),
            trade.getMakerOrderId()
        );
    }
}
