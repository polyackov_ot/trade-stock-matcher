package ot.polyackov.trade.stock.matcher.input.operation.converter;

public enum OrderOperationType {

    INSERT,
    UPDATE,
    CANCEL;


    public static boolean isInsert(OrderOperationType type) {
        return OrderOperationType.INSERT.equals(type);
    }

    public static boolean isUpdate(OrderOperationType type) {
        return OrderOperationType.UPDATE.equals(type);
    }

    public static boolean isCancel(OrderOperationType type) {
        return OrderOperationType.CANCEL.equals(type);
    }
}
