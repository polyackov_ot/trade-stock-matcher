package ot.polyackov.trade.stock.matcher.input;


import ot.polyackov.trade.stock.matcher.core.dto.operation.OrderOperation;
import ot.polyackov.trade.stock.matcher.input.operation.converter.CancelOrderOperationConverter;
import ot.polyackov.trade.stock.matcher.input.operation.converter.InsertOrderOperationConverter;
import ot.polyackov.trade.stock.matcher.input.operation.converter.OrderOperationType;
import ot.polyackov.trade.stock.matcher.input.operation.converter.UpdateOrderOperationConverter;

public class InputFromCsvConverter {

    private final InsertOrderOperationConverter insertConverter;
    private final UpdateOrderOperationConverter updateConverter;
    private final CancelOrderOperationConverter cancelConverter;

    public InputFromCsvConverter(InsertOrderOperationConverter insertConverter, UpdateOrderOperationConverter updateConverter, CancelOrderOperationConverter cancelConverter) {
        this.insertConverter = insertConverter;
        this.updateConverter = updateConverter;
        this.cancelConverter = cancelConverter;
    }


    public OrderOperation getOrderOperation(String operation) throws UnsupportedOperationException {
        if (operation.startsWith(OrderOperationType.INSERT.name())) {
            return insertConverter.convertToCommand(operation);
        }
        if (operation.startsWith(OrderOperationType.UPDATE.name())) {
            return updateConverter.convertToCommand(operation);
        }
        if (operation.startsWith(OrderOperationType.CANCEL.name())) {
            return cancelConverter.convertToCommand(operation);
        }

        throw new UnsupportedOperationException("The provided operation is not supported. Please ensure that the operation you're performing is either INSERT, UPDATE, or CANCEL.");
    }


}
