package ot.polyackov.trade.stock.matcher.output;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import ot.polyackov.trade.stock.matcher.core.dto.TradeAndActiveOrdersReport;

public class OutputToCsvConverter {

    /**
     * Convert {@link TradeAndActiveOrdersReport ResponseWrapper} to String format.
     *
     * <p>String format:
     * <pre>
     * {@code
     *  ===<symbol>===
     *  SELL,<ask_price>,<ask_volume>
     *  BUY,<bid_price>,<bid_volume>
     * }
     * E.g
     * {@code
     * FFLY,14.235,6,8,2
     * ===FFLY===
     * SELL,14.24,9
     * BUY,14.23,3
     * }
     * </pre>
     */
    public List<String> convertToCsv(TradeAndActiveOrdersReport report) {
        List<String> output = convertTrades(report);
        output.addAll(convertActiveOrders(report));

        return output;
    }

    private List<String> convertTrades(TradeAndActiveOrdersReport report) {
        return report.getTrades()
            .stream()
            .map(TradeConverter::convertToCsv)
            .collect(Collectors.toList());
    }

    private List<String> convertActiveOrders(TradeAndActiveOrdersReport report) {
        return report.getActiveOrders()
            .entrySet().stream()
            .map(entry -> OrderConverter.convertToCsv(entry.getKey(), entry.getValue()))
            .flatMap(Collection::stream)
            .collect(Collectors.toList());
    }
}
