package ot.polyackov.trade.stock.matcher.input.operation.converter;

import java.math.BigDecimal;
import ot.polyackov.trade.stock.matcher.core.dto.operation.UpdateOrderOperation;

public class UpdateOrderOperationConverter extends OrderOperationConverter {

    private static final int PRICE_INDEX = 2;
    private static final int VOLUME_INDEX = 3;


    /**
     * Convert an Operation command represented from CSV format into an {@link UpdateOrderOperation} object.
     *
     * <p>The CSV formatted command should follow the format below:
     * <pre>
     * {@code UPDATE,<order_id>,<price>,<volume>}
     * E.g
     * {@code "UPDATE,1,14.235,5"}
     * </pre>
     *
     * @param orderOperationString The string representation of the order operation.
     */
    public UpdateOrderOperation convertToCommand(String orderOperationString) {
        String[] array = splitOrderOperationString(orderOperationString);
        int orderId = getOrderId(array);
        BigDecimal price = new BigDecimal(array[PRICE_INDEX]);
        int volume = Integer.parseInt(array[VOLUME_INDEX]);

        return new UpdateOrderOperation(orderId, price, volume);
    }
}