package ot.polyackov.trade.stock.matcher.output;

import static java.lang.String.format;
import static java.util.stream.Collectors.summingInt;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import ot.polyackov.trade.stock.matcher.core.SideType;
import ot.polyackov.trade.stock.matcher.core.dto.Order;


public class OrderConverter {

    /**
     * Convert {@link Order ActiveOrder} to String format.
     *
     * <p>For orders with equal {@link Order#getSymbol() symbols}  and {@link Order#getPrice() prices}, the {@link Order#getVolume() volumes} should be summarised.
     *
     * <p>String format:
     * <pre>
     * {@code
     *  <symbol>,<price>,<volume>,<taker_order_id>,<maker_order_id>
     *  ===<symbol>===
     *  SELL,<ask_price>,<ask_volume>
     *  BUY,<bid_price>,<bid_volume>
     * }
     * E.g
     * {@code
     * ===FFLY===
     * SELL,14.24,9
     * BUY,14.23,3
     * }
     * </pre>
     */
    public static List<String> convertToCsv(String symbol, List<Order> activeOrders) {
        List<String> ordersBySymbol = new ArrayList<>();
        ordersBySymbol.add(format("===%s===", symbol));
        List<String> activeOrderStrings = activeOrders.stream()
            .collect(Collectors.groupingBy(OrderGroupByKey::new, summingInt(Order::getVolume)))
            .entrySet().stream()
            .map(entry -> new OrderAggregateBySideAndPrice(entry.getKey().getSide(), entry.getKey().getPrice(), entry.getValue()))
            .sorted(Comparator.comparing(OrderAggregateBySideAndPrice::getSide).thenComparing(OrderAggregateBySideAndPrice::getPrice).reversed())
            .map(order -> format("%s,%s,%d",
                order.getSide(),
                order.getPrice(),
                order.getVolumeSum()))
            .collect(Collectors.toList());

        ordersBySymbol.addAll(activeOrderStrings);
        return ordersBySymbol;
    }

    private static class OrderGroupByKey {
        private SideType side;
        private BigDecimal price;

        public OrderGroupByKey(Order order) {
            this.side = order.getSide();
            this.price = order.getPrice();
        }

        public SideType getSide() {
            return side;
        }

        public BigDecimal getPrice() {
            return price;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            OrderGroupByKey that = (OrderGroupByKey) o;
            return side == that.side && Objects.equals(price, that.price);
        }

        @Override
        public int hashCode() {
            int result = Objects.hashCode(side);
            result = 31 * result + Objects.hashCode(price);
            return result;
        }
    }


    public static class OrderAggregateBySideAndPrice {
        private SideType side;
        private BigDecimal price;
        private int volumeSum;

        public OrderAggregateBySideAndPrice(SideType side, BigDecimal price, int volumeSum) {
            this.side = side;
            this.price = price;
            this.volumeSum = volumeSum;
        }

        public SideType getSide() {
            return side;
        }

        public BigDecimal getPrice() {
            return price;
        }

        public int getVolumeSum() {
            return volumeSum;
        }

    }
}
