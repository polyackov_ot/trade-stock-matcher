package ot.polyackov.trade.stock.matcher.core.dto.operation;

import java.math.BigDecimal;
import ot.polyackov.trade.stock.matcher.input.operation.converter.OrderOperationType;

public class UpdateOrderOperation extends OrderOperation {

    private BigDecimal price;

    private int volume;

    public UpdateOrderOperation(int orderId, BigDecimal price, int volume) {
        super(OrderOperationType.UPDATE, orderId);
        validatePrice(price);
        //validateVolume(volume);
        this.price = price;
        this.volume = volume;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public int getVolume() {
        return volume;
    }

}
