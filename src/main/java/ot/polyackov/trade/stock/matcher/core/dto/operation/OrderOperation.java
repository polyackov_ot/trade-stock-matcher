package ot.polyackov.trade.stock.matcher.core.dto.operation;

import static java.lang.String.format;

import java.math.BigDecimal;
import ot.polyackov.trade.stock.matcher.input.operation.converter.OrderOperationType;

public abstract class OrderOperation {

    private final OrderOperationType type;

    private final int orderId;

    public OrderOperation(OrderOperationType type, int orderId) {
        this.type = type;
        this.orderId = orderId;
    }

    protected static void validateVolume(int volume) {
        if (volume < 0) {
            throw new IllegalArgumentException(format("Expected volume should be positive. Actual volume is '%d'.", volume));
        }
    }

    private static final int MAX_SCALE_PRICE = 4;

    protected static void validatePrice(BigDecimal price) {
        if (BigDecimal.ZERO.compareTo(price) > 0) {
            throw new IllegalArgumentException(format("Expected price should be positive. Actual price is '%s'.", price.toString()));
        }
        if (price.scale() > MAX_SCALE_PRICE) {
            throw new IllegalArgumentException(String.format("A price is a string with a maximum of %d digits behind the \".\"", MAX_SCALE_PRICE));
        }
    }

    public OrderOperationType getType() {
        return type;
    }

    public int getOrderId() {
        return orderId;
    }

}
