package ot.polyackov.trade.stock.matcher.input.operation.converter;

import java.math.BigDecimal;
import ot.polyackov.trade.stock.matcher.core.SideType;
import ot.polyackov.trade.stock.matcher.core.dto.operation.InsertOrderOperation;

public class InsertOrderOperationConverter extends OrderOperationConverter {

    private static final int SYMBOL_INDEX = 2;
    private static final int SIDE_INDEX = 3;
    private static final int PRICE_INDEX = 4;
    private static final int VOLUME_INDEX = 5;


    /**
     * Convert an Operation command represented from CSV format into an {@link InsertOrderOperation} object.
     *
     * <p>The CSV formatted command should follow the format below:
     * <pre>
     * {@code INSERT,<order_id>,<symbol>,<side>,<price>,<volume>}
     * E.g
     * {@code "INSERT,1,FFLY,BUY,14.235,5"}
     * </pre>
     *
     * @param orderOperationString The string representation of the order operation.
     */
    public InsertOrderOperation convertToCommand(String orderOperationString) {
        String[] array = splitOrderOperationString(orderOperationString);
        int orderId = getOrderId(array);
        String symbol = array[SYMBOL_INDEX];
        SideType side = SideType.valueOf(array[SIDE_INDEX]);
        BigDecimal price = new BigDecimal(array[PRICE_INDEX]);
        int volume = Integer.parseInt(array[VOLUME_INDEX]);

        return new InsertOrderOperation(orderId, symbol, side, price, volume);
    }
}
