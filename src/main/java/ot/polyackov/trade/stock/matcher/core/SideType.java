package ot.polyackov.trade.stock.matcher.core;

public enum SideType {

    BUY,
    SELL;

    public static boolean isBuy(SideType side) {
        return SideType.BUY.equals(side);
    }

}

