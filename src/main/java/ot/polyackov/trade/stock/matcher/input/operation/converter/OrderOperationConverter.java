package ot.polyackov.trade.stock.matcher.input.operation.converter;


public abstract class OrderOperationConverter {

    private static final String CSV_SEPARATOR = ",";

    private static final int ORDER_ID_INDEX = 1;

    String[] splitOrderOperationString(String orderOperationString) {
        return orderOperationString.split(CSV_SEPARATOR);
    }

    int getOrderId(String[] array) {
        return Integer.parseInt(array[ORDER_ID_INDEX]);
    }
}
