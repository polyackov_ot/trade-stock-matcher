package ot.polyackov.trade.stock.matcher.core.dto;


import java.util.List;
import java.util.Map;


public class TradeAndActiveOrdersReport {

    private List<Trade> trades;
    private Map<String, List<Order>> activeOrders;

    public TradeAndActiveOrdersReport(List<Trade> trades, Map<String, List<Order>> activeOrders) {
        this.trades = trades;
        this.activeOrders = activeOrders;
    }

    public List<Trade> getTrades() {
        return trades;
    }

    public Map<String, List<Order>> getActiveOrders() {
        return activeOrders;
    }


}