package ot.polyackov.trade.stock.matcher.core;

import static java.util.stream.Collectors.groupingBy;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Objects;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import ot.polyackov.trade.stock.matcher.core.dto.Order;
import ot.polyackov.trade.stock.matcher.core.dto.Trade;
import ot.polyackov.trade.stock.matcher.core.dto.TradeAndActiveOrdersReport;
import ot.polyackov.trade.stock.matcher.core.dto.operation.CancelOrderOperation;
import ot.polyackov.trade.stock.matcher.core.dto.operation.InsertOrderOperation;
import ot.polyackov.trade.stock.matcher.core.dto.operation.OrderOperation;
import ot.polyackov.trade.stock.matcher.core.dto.operation.UpdateOrderOperation;
import ot.polyackov.trade.stock.matcher.input.operation.converter.OrderOperationType;


public class TradeMatchingServiceImpl implements TradeMatchingService {

    private static final NavigableMap<PriceAndSerialNumberKey, Order> EMPTY_TREE_MAP = new TreeMap<>();

    /**
     * {@link TradeMatchingServiceImpl#buyOrderBook} is {@code Map<Symbol, TreeMap<Price & serialNumber, Order>>}.
     *
     * <p>It is used for getting a suitable buy order list which does not need to be sorted.
     *
     * @see #generateNewTreeMapForOrders
     */
    private final Map<String, NavigableMap<PriceAndSerialNumberKey, Order>> buyOrderBook;
    /**
     * {@link TradeMatchingServiceImpl#sellOrderBook} is {@code Map<Symbol, TreeMap<Price & serialNumber, Order>>}.
     *
     * <p>It is used for getting a suitable buy order list which does not need to be sorted.
     *
     * @see #generateNewTreeMapForOrders
     */
    private final Map<String, NavigableMap<PriceAndSerialNumberKey, Order>> sellOrderBook;
    /**
     * {@link TradeMatchingServiceImpl#orderIdAndOrderMap} is {@code Map<orderId, Order>}.
     *
     * <p>It is used for quick search order by orderId.
     */
    private final Map<Integer, Order> orderIdAndOrderMap;
    private final List<Trade> trades;
    private final AtomicInteger index;


    public TradeMatchingServiceImpl() {
        buyOrderBook = new HashMap<>();
        sellOrderBook = new HashMap<>();
        orderIdAndOrderMap = new HashMap<>();
        trades = new ArrayList<>();
        index = new AtomicInteger(1);
    }

    private Map<String, NavigableMap<PriceAndSerialNumberKey, Order>> getOrdersBySideType(SideType sideType) {
        return SideType.isBuy(sideType) ? buyOrderBook : sellOrderBook;
    }


    @Override
    public void runMatchingEngine(OrderOperation orderOperation) {
        if (OrderOperationType.isInsert(orderOperation.getType())) {
            insertOrder((InsertOrderOperation) orderOperation);
        }
        if (OrderOperationType.isUpdate(orderOperation.getType())) {
            updateOrder((UpdateOrderOperation) orderOperation);
        }
        if (OrderOperationType.isCancel(orderOperation.getType())) {
            cancelOrder((CancelOrderOperation) orderOperation);
        }
    }

    @Override
    public void insertOrder(InsertOrderOperation insert) {
        Order order = new Order(
            insert.getOrderId(),
            insert.getSymbol(),
            insert.getSide(),
            insert.getPrice(),
            insert.getVolume()
        );

        matchTradeOrAddInOrderBook(order);
    }


    private void matchTradeOrAddInOrderBook(Order order) {
        order.setSerialNumber(index.getAndIncrement());

        if (SideType.SELL.equals(order.getSide())) {
            matchTradeOrAddInOrderBook(order, buyOrderBook);

            manageTakerOrder(order, sellOrderBook);
        }
        if (SideType.BUY.equals(order.getSide())) {
            matchTradeOrAddInOrderBook(order, sellOrderBook);

            manageTakerOrder(order, buyOrderBook);
        }
    }

    private void matchTradeOrAddInOrderBook(Order takerOrder, Map<String, NavigableMap<PriceAndSerialNumberKey, Order>> makerOrdersBook) {
        NavigableMap<PriceAndSerialNumberKey, Order> makerOrdersBySymbol = makerOrdersBook.getOrDefault(takerOrder.getSymbol(), EMPTY_TREE_MAP);
        Map<PriceAndSerialNumberKey, Order> suitableMakerOrders = getSuitableMakerOrders(takerOrder, makerOrdersBook);

        List<PriceAndSerialNumberKey> keysToRemove = new ArrayList<>();
        for (Map.Entry<PriceAndSerialNumberKey, Order> entry : suitableMakerOrders.entrySet()) {
            Order makerOrder = entry.getValue();

            int tradeVolume = Math.min(takerOrder.getVolume(), makerOrder.getVolume());
            takerOrder.setVolume(takerOrder.getVolume() - tradeVolume);
            makerOrder.setVolume(makerOrder.getVolume() - tradeVolume);
            if (makerOrder.getVolume() == 0) {
                orderIdAndOrderMap.remove(makerOrder.getOrderId());
                keysToRemove.add(entry.getKey());
            }

            trades.add(new Trade(makerOrder.getPrice(), takerOrder.getSymbol(), tradeVolume, takerOrder.getOrderId(), makerOrder.getOrderId()));
            if (takerOrder.getVolume() == 0) {
                break;
            }
        }

        removeEmptyOrder(makerOrdersBySymbol, keysToRemove);
    }

    private static Map<PriceAndSerialNumberKey, Order> getSuitableMakerOrders(Order order, Map<String, NavigableMap<PriceAndSerialNumberKey, Order>> takerOrders) {
        PriceAndSerialNumberKey borderKey = PriceAndSerialNumberKey.createWithMaxSerialNumber(order);
        NavigableMap<PriceAndSerialNumberKey, Order> makerOrdersBySymbol = takerOrders.getOrDefault(order.getSymbol(), EMPTY_TREE_MAP);
        return makerOrdersBySymbol.headMap(borderKey);
    }

    private void removeEmptyOrder(NavigableMap<PriceAndSerialNumberKey, Order> makerOrdersBySymbol, List<PriceAndSerialNumberKey> keysToRemove) {
        for (PriceAndSerialNumberKey keyToRemove : keysToRemove) {
            makerOrdersBySymbol.remove(keyToRemove);
        }
    }

    private void manageTakerOrder(Order takerOrder, Map<String, NavigableMap<PriceAndSerialNumberKey, Order>> takerOrders) {
        if (takerOrder.getVolume() > 0) {
            String symbol = takerOrder.getSymbol();
            NavigableMap<PriceAndSerialNumberKey, Order> ordersForSymbol = takerOrders.getOrDefault(symbol, generateNewTreeMapForOrders(takerOrder.getSide()));

            PriceAndSerialNumberKey key = new PriceAndSerialNumberKey(takerOrder);
            ordersForSymbol.put(key, takerOrder);
            takerOrders.put(symbol, ordersForSymbol);

            orderIdAndOrderMap.put(takerOrder.getOrderId(), takerOrder);
        }
    }

    /**
     * Create TreeMap with special comparators for {@link SideType#BUY BUY} and {@link SideType#SELL SELL} allow the matching algorithm to receive a list of orders
     * in the required sequence, removing unnecessary load and reducing the sorting time complexity each time you try to make a {@link Trade}.
     *
     * <p>The {@link SideType#SELL SELL} order should match the {@link SideType#BUY BUY} order with the highest price and the lowest serial Number.
     * => {@link TradeMatchingServiceImpl#buyOrderBook} should sort by {@code price DESC & serialNumber ASC}.
     *
     * <p>The {@link SideType#BUY BUY} order should match the {@link SideType#SELL SELL} order with the lowest price and the lowest serial Number.
     * => {@link TradeMatchingServiceImpl#sellOrderBook} should sort by {@code price ASC & serialNumber ASC}.
     *
     * @return treeMap with special comparator
     */
    private NavigableMap<PriceAndSerialNumberKey, Order> generateNewTreeMapForOrders(SideType sideType) {
        if (SideType.isBuy(sideType)) {
            return new TreeMap<>(Comparator.comparing(PriceAndSerialNumberKey::getPrice).reversed()
                .thenComparingInt(PriceAndSerialNumberKey::getSerialNumber));
        } else {
            return new TreeMap<>(Comparator.comparing(PriceAndSerialNumberKey::getPrice)
                .thenComparingInt(PriceAndSerialNumberKey::getSerialNumber));
        }
    }

    @Override
    public void updateOrder(UpdateOrderOperation update) {
        if (update.getVolume() <= 0) {
            return;
        }

        Order order = orderIdAndOrderMap.get(update.getOrderId());
        if (order == null) {
            return;
        }
        if (shouldChangePriority(update, order)) {
            Map<String, NavigableMap<PriceAndSerialNumberKey, Order>> orderBook = getOrdersBySideType(order.getSide());
            NavigableMap<PriceAndSerialNumberKey, Order> ordersForSymbol = orderBook.get(order.getSymbol());
            if (ordersForSymbol != null) {
                cancelOrder(update.getOrderId());

                order.setPrice(update.getPrice());
                order.setVolume(update.getVolume());

                matchTradeOrAddInOrderBook(order);
            }
        } else {
            order.setPrice(update.getPrice());
            order.setVolume(update.getVolume());
        }
    }

    private static boolean shouldChangePriority(UpdateOrderOperation update, Order order) {
        return order.getPrice().compareTo(update.getPrice()) != 0
            || order.getVolume() < update.getVolume();
    }

    @Override
    public void cancelOrder(CancelOrderOperation cancel) {
        int orderId = cancel.getOrderId();
        cancelOrder(orderId);
    }

    private void cancelOrder(int orderId) {
        Order order = orderIdAndOrderMap.get(orderId);
        if (order != null) {
            orderIdAndOrderMap.remove(orderId);
            Map<String, NavigableMap<PriceAndSerialNumberKey, Order>> orderBook = getOrdersBySideType(order.getSide());
            NavigableMap<PriceAndSerialNumberKey, Order> ordersForSymbol = orderBook.get(order.getSymbol());
            if (ordersForSymbol != null) {
                PriceAndSerialNumberKey key = new PriceAndSerialNumberKey(order);
                ordersForSymbol.remove(key);
                if (ordersForSymbol.isEmpty()) {
                    orderBook.remove(order.getSymbol());
                }
            }
        }
    }


    @Override
    public TradeAndActiveOrdersReport getTradeAndActiveOrdersReport() {
        Map<String, List<Order>> activeOrders = orderIdAndOrderMap.values().stream()
            .filter(entry -> entry.getVolume() > 0)
            .collect(groupingBy(Order::getSymbol));

        return new TradeAndActiveOrdersReport(trades, activeOrders);
    }


    private static class PriceAndSerialNumberKey implements Comparable<PriceAndSerialNumberKey> {

        private final BigDecimal price;
        private final int serialNumber;

        public PriceAndSerialNumberKey(Order order) {
            this.price = order.getPrice();
            this.serialNumber = order.getSerialNumber();
        }

        public PriceAndSerialNumberKey(Order order, int serialNumber) {
            this.price = order.getPrice();
            this.serialNumber = serialNumber;
        }

        public static PriceAndSerialNumberKey createWithMaxSerialNumber(Order order) {
            return new PriceAndSerialNumberKey(order, Integer.MAX_VALUE);
        }

        public BigDecimal getPrice() {
            return price;
        }

        public int getSerialNumber() {
            return serialNumber;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            PriceAndSerialNumberKey that = (PriceAndSerialNumberKey) o;
            return serialNumber == that.serialNumber && Objects.equals(price, that.price);
        }

        @Override
        public int hashCode() {
            int result = Objects.hashCode(price);
            result = 31 * result + serialNumber;
            return result;
        }

        @Override
        public int compareTo(PriceAndSerialNumberKey key) {
            return Integer.compare(key.serialNumber, serialNumber);
        }

    }
}

