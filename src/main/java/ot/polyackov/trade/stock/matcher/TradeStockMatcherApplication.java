package ot.polyackov.trade.stock.matcher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class TradeStockMatcherApplication {

    public static void main(String[] args) {
        SpringApplication.run(TradeStockMatcherApplication.class, args);
    }

}
