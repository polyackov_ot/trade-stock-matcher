package ot.polyackov.trade.stock.matcher.core.dto.operation;

import java.math.BigDecimal;
import ot.polyackov.trade.stock.matcher.core.SideType;
import ot.polyackov.trade.stock.matcher.input.operation.converter.OrderOperationType;

public class InsertOrderOperation extends OrderOperation {

    private final String symbol;

    private final SideType side;

    private BigDecimal price;

    private int volume;


    public InsertOrderOperation(int orderId, String symbol, SideType side, BigDecimal price, int volume) {
        super(OrderOperationType.INSERT, orderId);
        validatePrice(price);
        validateVolume(volume);
        this.symbol = symbol;
        this.side = side;
        this.price = price;
        this.volume = volume;
    }


    public String getSymbol() {
        return symbol;
    }

    public int getVolume() {
        return volume;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public SideType getSide() {
        return side;
    }

}
