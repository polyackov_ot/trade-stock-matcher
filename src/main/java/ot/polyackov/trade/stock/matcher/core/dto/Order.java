package ot.polyackov.trade.stock.matcher.core.dto;

import java.math.BigDecimal;
import ot.polyackov.trade.stock.matcher.core.SideType;

public class Order {

    private final int orderId;

    private final String symbol;

    private final SideType side;

    private BigDecimal price;

    private int volume;

    private int serialNumber;


    public Order(int orderId, String symbol, SideType side, BigDecimal price, int volume) {
        this.orderId = orderId;
        this.symbol = symbol;
        this.side = side;
        this.price = price;
        this.volume = volume;
    }

    public int getOrderId() {
        return orderId;
    }

    public String getSymbol() {
        return symbol;
    }

    public SideType getSide() {
        return side;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

}
