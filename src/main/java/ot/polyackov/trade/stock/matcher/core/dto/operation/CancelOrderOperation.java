package ot.polyackov.trade.stock.matcher.core.dto.operation;


import ot.polyackov.trade.stock.matcher.input.operation.converter.OrderOperationType;

public class CancelOrderOperation extends OrderOperation {

    public CancelOrderOperation(int orderId) {
        super(OrderOperationType.CANCEL, orderId);
    }
}
