package ot.polyackov.trade.stock.matcher;

import java.util.List;
import ot.polyackov.trade.stock.matcher.core.TradeMatchingService;
import ot.polyackov.trade.stock.matcher.core.TradeMatchingServiceImpl;
import ot.polyackov.trade.stock.matcher.core.dto.TradeAndActiveOrdersReport;
import ot.polyackov.trade.stock.matcher.core.dto.operation.OrderOperation;
import ot.polyackov.trade.stock.matcher.input.InputFromCsvConverter;
import ot.polyackov.trade.stock.matcher.input.operation.converter.CancelOrderOperationConverter;
import ot.polyackov.trade.stock.matcher.input.operation.converter.InsertOrderOperationConverter;
import ot.polyackov.trade.stock.matcher.input.operation.converter.UpdateOrderOperationConverter;
import ot.polyackov.trade.stock.matcher.output.OutputToCsvConverter;

public class Machine {

    public static List<String> runMatchingEngine(List<String> operations) {
        InputFromCsvConverter inputConverter = new InputFromCsvConverter(
            new InsertOrderOperationConverter(),
            new UpdateOrderOperationConverter(),
            new CancelOrderOperationConverter()
        );

        TradeMatchingService tradeMatchingService = new TradeMatchingServiceImpl();

        for (String operation : operations) {
            try {
                OrderOperation orderOperation = inputConverter.getOrderOperation(operation);

                tradeMatchingService.runMatchingEngine(orderOperation);


            } catch (UnsupportedOperationException ex) {
                //todo
            }
        }

        TradeAndActiveOrdersReport report = tradeMatchingService.getTradeAndActiveOrdersReport();

        OutputToCsvConverter outputToCsvConverter = new OutputToCsvConverter();
        return outputToCsvConverter.convertToCsv(report);
    }

}
