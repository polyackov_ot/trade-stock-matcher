package ot.polyackov.trade.stock.matcher.core;

import ot.polyackov.trade.stock.matcher.core.dto.TradeAndActiveOrdersReport;
import ot.polyackov.trade.stock.matcher.core.dto.operation.CancelOrderOperation;
import ot.polyackov.trade.stock.matcher.core.dto.operation.InsertOrderOperation;
import ot.polyackov.trade.stock.matcher.core.dto.operation.OrderOperation;
import ot.polyackov.trade.stock.matcher.core.dto.operation.UpdateOrderOperation;

public interface TradeMatchingService {

    void runMatchingEngine(OrderOperation orderOperation);

    void insertOrder(InsertOrderOperation insert);

    void updateOrder(UpdateOrderOperation update);

    void cancelOrder(CancelOrderOperation cancel);

    /**
     * Generate a report with all trades and active orders.
     *
     * @return TradeAndActiveOrdersReport report
     */
    TradeAndActiveOrdersReport getTradeAndActiveOrdersReport();
}
