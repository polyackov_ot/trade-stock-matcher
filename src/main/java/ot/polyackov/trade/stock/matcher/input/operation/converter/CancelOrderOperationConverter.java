package ot.polyackov.trade.stock.matcher.input.operation.converter;


import ot.polyackov.trade.stock.matcher.core.dto.operation.CancelOrderOperation;

public class CancelOrderOperationConverter extends OrderOperationConverter {

    /**
     * Convert an Operation command represented from CSV format into an {@link CancelOrderOperation} object.
     *
     * <p>The CSV formatted command should follow the format below:
     * <pre>
     * {@code CANCEL,<order_id>}
     * E.g
     * {@code "CANCEL,1"}
     * </pre>
     *
     * @param orderOperationString The string representation of the order operation.
     */
    public CancelOrderOperation convertToCommand(String orderOperationString) {
        String[] array = splitOrderOperationString(orderOperationString);
        int orderId = getOrderId(array);

        return new CancelOrderOperation(orderId);
    }
}