package ot.polyackov.trade.stock.matcher.core.dto;

import java.math.BigDecimal;

public class Trade {

    private String symbol;

    private BigDecimal price;

    private int volume;

    private int takerOrderId;

    private int makerOrderId;

    public Trade(BigDecimal price, String symbol, int volume, int takerOrderId, int makerOrderId) {
        this.price = price;
        this.symbol = symbol;
        this.volume = volume;
        this.takerOrderId = takerOrderId;
        this.makerOrderId = makerOrderId;
    }

    public String getSymbol() {
        return symbol;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public int getVolume() {
        return volume;
    }

    public int getTakerOrderId() {
        return takerOrderId;
    }

    public int getMakerOrderId() {
        return makerOrderId;
    }

}
